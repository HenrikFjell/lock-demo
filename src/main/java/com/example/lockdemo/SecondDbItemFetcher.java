package com.example.lockdemo;

import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Future;

@Service
public class SecondDbItemFetcher {

  private final DbItemRepository repository;
  private final Logger logger;

  public SecondDbItemFetcher(DbItemRepository repository, Logger logger) {
    this.repository = repository;
    this.logger = logger;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DbItem getItemUnlocked(long primaryKey) {
    return repository.findAll().stream()
        .filter((dbItem -> dbItem.getPrimaryKey() == primaryKey))
        .findFirst()
        .orElse(new DbItem());
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DbItem getItemLocked(long primaryKey) {
    DbItem dbItem = null;
    logger.info("Trying to lock");
    try {
      dbItem = repository.findDbItemByPrimaryKey(primaryKey).orElse(new DbItem());
    } catch (Exception e) {
      logger.warn("Got exception: {}", e.getClass().getName());
    }
    return dbItem;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  @Async("dbThread")
  public Future<DbItem> getItemLockedAsync(long primaryKey) {
    logger.info("Start waiting for lock");
    DbItem result = repository.findDbItemByPrimaryKey(primaryKey).orElse(new DbItem());
    logger.info("Got lock");
    return new AsyncResult<>(result);
  }
}
