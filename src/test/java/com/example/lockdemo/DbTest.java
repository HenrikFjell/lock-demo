package com.example.lockdemo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DbTest {

  @Autowired private DbItemRepository dbItemRepository;
  @Autowired private DbItemFetcher dbItemFetcher;
  @Autowired private MultiFetcher multiFetcher;

  @BeforeEach
  void initDb() {
    for (int i = 0; i < 10; i++) {
      dbItemRepository.save(new DbItem("test" + i));
    }
    TestTransaction.flagForCommit();
    TestTransaction.end();
  }

  @AfterEach
  void resetDb() {
    dbItemRepository.deleteAll();
  }

  @Test
  public void fetchUnlocked() {
    DbItem dbItem = dbItemFetcher.fetchUnlocked(1L);

    assertEquals("test0", dbItem.getValue());
  }

  @Test
  public void fetchLocked() {
    DbItem dbItem = dbItemFetcher.fetchLocked(1L);

    assertEquals("test0", dbItem.getValue());
  }

  @Test
  void update() throws ExecutionException, InterruptedException {
    DbItem dbItem= multiFetcher.updateAndCheck(1L);

    assertTrue(dbItem.isUpdated());
  }
}
