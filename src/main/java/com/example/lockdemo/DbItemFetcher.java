package com.example.lockdemo;

import org.slf4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Future;

@Service
public class DbItemFetcher {

  private final DbItemRepository repository;
  private final SecondDbItemFetcher secondDbItemFetcher;
  private final Logger logger;

  public DbItemFetcher(
      DbItemRepository repository, SecondDbItemFetcher secondDbItemFetcher, Logger logger) {
    this.repository = repository;
    this.secondDbItemFetcher = secondDbItemFetcher;
    this.logger = logger;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DbItem fetchUnlocked(long primaryKey) {
    DbItem lockedDbItem = repository.findDbItemByPrimaryKey(primaryKey).orElse(new DbItem());

    return secondDbItemFetcher.getItemUnlocked(primaryKey);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DbItem fetchLocked(long primaryKey) {
    DbItem lockedDbItem = repository.findDbItemByPrimaryKey(primaryKey).orElse(new DbItem());
    logger.info("Item locked");
    return secondDbItemFetcher.getItemLocked(primaryKey);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  @Async("dbThread")
  public Future<Void> fetchLockedAndUpdate(long primaryKey) {
    DbItem lockedDbItem = repository.findDbItemByPrimaryKey(primaryKey).orElse(new DbItem());
    logger.info("Locked for update");
    lockedDbItem.setUpdated(true);
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    logger.info("Update ready");
    return null;
  }
}
