package com.example.lockdemo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface DbItemRepository extends JpaRepository<DbItem, Long> {

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  public Optional<DbItem> findDbItemByPrimaryKey(Long primaryId);
}
