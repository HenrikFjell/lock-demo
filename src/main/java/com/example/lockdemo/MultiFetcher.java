package com.example.lockdemo;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class MultiFetcher {

  private final DbItemFetcher dbItemFetcher;
  private final SecondDbItemFetcher secondDbItemFetcher;
  private final Logger logger;

  public MultiFetcher(DbItemFetcher dbItemFetcher, SecondDbItemFetcher secondDbItemFetcher, Logger logger) {
    this.dbItemFetcher = dbItemFetcher;
    this.secondDbItemFetcher = secondDbItemFetcher;
    this.logger = logger;
  }

  public DbItem updateAndCheck(long primaryKey) throws InterruptedException, ExecutionException {
    dbItemFetcher.fetchLockedAndUpdate(primaryKey);
    logger.info("update started");
    Thread.sleep(200);
    Future<DbItem> futureCheck = secondDbItemFetcher.getItemLockedAsync(primaryKey);
    logger.info("check started");
    return futureCheck.get();
  }
}
